# README #

# Introduction to Data Science: Week 1

If not provided with a tweet file, use `python twitterstream.py > output.txt` (UNIX)
filled with your credentials. 
Don't have credentials? Go to [twitter dev] (https://dev.twitter.com/apps)

### What is this repository for? 

* Assignments for week 1 of the Coursera course "Introduction to Data Science" 

### How do I get set up? ###

* For each python file, you can run it by either giving it execution permissions `chmod +x FILE.py [ARGS]` or invoking the python interpreter `python FILE.py `

##### For further info:
* [Intro to Data Science Course](https://class.coursera.org/datasci-002)
* duckduckgo, bing or google,... are your friends